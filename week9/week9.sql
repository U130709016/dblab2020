create or replace view usa_customers as				#if it exist it replaced with the new one
select CustomerID,CustomerName,ContactName
from customers
where Country = "USA";
call usa_customers ;

select * from usa_customers join orders on usa_customers.CustomerID = orders.CustomerID ;

select ProductID , ProductName , Price
from products
where Price < (select avg(Price) from products) ;

create or replace view less_avg_products as 
select ProductID , ProductName , Price
from products
where Price < (select avg(Price) from products) ;
call less_avg_products ;

select * from less_avg_products ;
